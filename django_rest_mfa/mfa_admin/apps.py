from django.apps import AppConfig


class MfaAdminConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "django_rest_mfa.mfa_admin"
