# Django REST MFA

Now that [django-allauth](https://github.com/pennersr/django-allauth) supports an API and MFA - there is no need for this project. You can migrate to allauth with:

```python
def migrate_mfa(apps, schema_editor):
    UserKey = apps.get_model("django_rest_mfa", "UserKey")
    Authenticator = apps.get_model("mfa", "Authenticator")

    adapter = get_adapter()
    authenticators = []
    for totp in UserKey.objects.filter(key_type="TOTP").iterator():
        recovery_codes = set()
        for backup_code in UserKey.objects.filter(
            key_type="Backup Codes", user_id=totp.user_id
        ):
            recovery_codes.update(backup_code.properties["codes"])
        secret = totp.properties["secret_key"]
        authenticators.append(
            Authenticator(
                user_id=totp.user_id,
                type="totp",
                data={"secret": adapter.encrypt(secret)},
            )
        )
        if recovery_codes:
            authenticators.append(
                Authenticator(
                    user_id=totp.user_id,
                    type="recovery_codes",
                    data={
                        "migrated_codes": [adapter.encrypt(c) for c in recovery_codes],
                    },
                )
            )
    Authenticator.objects.bulk_create(authenticators)
```

FIDO2 and OTP Multi-factor authentication endpoints built in Django Rest Framework

Focuses on a small but high quality feature set. May be dropped in or used as a reference solution. This project is based on the work of https://github.com/mkalioby/django-mfa2/ Thanks for the reference! Instead of using Django templates, it uses DRF API Endpoints and is suitable for single page web and native apps.

# Features

- FIDO2 via python-fido2
- OTP via pyotp
- Session based MFA authentication
- Django Admin integration - login and MFA management
- Remember devices
- Backup codes

![](image1.png)
![](image2.png)

# Install

Use pip or poetry to install TODO

Add to urls

```
    path("mfa/", include("django_rest_mfa.urls")),
```

Add to settings.py INSTALLED_APPS

```
    "django_rest_mfa",
```

## Prerequisites

- Django Rest Framework
- Django Sessions (You may use auth tokens in addition, but sessions are necessary, at this time, to "remember" which step of authentication a user is on)

## Integration

Django REST MFA is not a drop in application. See the included mfa_admin as one example of how to integrate.

Having multiple authentication steps complicates the login process. I suggest one of two strategies.

- Replace ALL login views with special login views that check for MFA requirements. This lets the user be entirely logged in once finished. It's simple, but be careful not to leave other login views that might bypass it.
- Allow the user to log in as normal, but check MFA permissions on some views. This is good if you'd like the user to access a limited set of views without MFA. By default, rest-mfa will mark the session already with MFA info. It would work fine to add data to your auth tokens as well to note the MFA status.

In your login view, call the `django_rest_auth.helpers.has_mfa` function. When false, the user does not have MFA enabled and should continue logging in normally. Otherwise this functions will mark the user's session with their user PK, which notes them as having authenticated via username/password. It returns the user's UserKey QuerySet which could be used to determine what the user needs to do next.

### Supported MFA

- TOTP (Time based one time pass)
- FIDO2 (Yubikeys and other hardware keys)
- Trusted Device (Remember / Don't ask again for this device).
- Backup Codes (One time use codes, useful if TOTP key is lost)

### API

API endpoints are documented via Django Rest Framework's built in browsable API. After installing, open the API route in a browser to view. If you set the base url to "mfa" then the endpoints will be

- /mfa/user_keys/ - ViewSet for CRUD operations around user keys. Make note of extra actions to create.
- /mfa/authenticate/<totp/fido2/backup>/ - Second factor authentication endpoints. Use these to log in with a second factor after your initial login.

Many endpoints follow a strategy of GET to view some needed information, then POST to commit it. See each endpoint in the browsable API for more info.

## Admin Integration

Add mfa_admin urls BEFORE site admin urls. This is important so as to override the Django Admin Login.

```
    path("admin/", include("django_rest_mfa.mfa_admin.urls")),
    path("admin/", admin.site.urls)
```

Add to settings.py INSTALLED_APPS

```
    "django_rest_mfa",
    "django_rest_mfa.mfa_admin",
```

Additionally, place mfa_admin above `django.contrib.admin` if you would like to have a link to multi factor authentication appear in all django admin templates.

Admin users can manage their MFA at `/admin/multi_factor_auth/`

Admin integration works but has a proof of concept feel to the UI. Please consider contributing to polish this before using it in a production environment.

### dj-rest-auth helper

If using session authentication (not token/jwt) and dj-rest-auth we provide a drop in helper login view

```
from django_rest_mfa.rest_auth_helpers.views import MFALoginView

...

    path("rest-auth/login/", MFALoginView.as_view()),
    path("rest-auth/", include("dj_rest_auth.urls")),
```

When a user has MFA disabled, the MFALoginView will act like dj-rest-auth's LoginView. That makes it safe to drop in for backwards compatibility. When a user has MFA enabled, this view will mark the user's session for MFA verification but it will not log in the user. It will return a status code of 200 and JSON `{"requires_mfa": true}`. At this point you should redirect your user to a MFA Login page to continue. 

This does not work with token authentication. You need to build this yourself.

## Settings

- FIDO_SERVER_ID - FIDO2 Server ID. Example: `localhost`
- MFA_SERVER_NAME = Generic MFA Server name. Example: "My Project". Used to denote which application the key is for, such as viewing in a TOTP Authenticator app.
- MFA_DEVICE_COOKIE_NAME = Cookie name for setting remember device keys. Default `remember_device_key`
- MFA_DEVICE_MAX_AGE_DAYS = Max age in days to remember device. Default `90`

## Extras

See the `rest_auth_helpers` directory for tooling around integrating with `dj-rest-auth`.

# Local development

1. Create virtual environment `python -m venv env` and `source env/bin/activate`
1. Install dependencies with [Poetry](https://python-poetry.org/) `poetry install`
1. Run with SSL `./manage.py runsslserver`

Know a way to perform FIDO2 auth on localhost without SSL? Submit a fix, please! It's documented as supported but I could not get it to work.

# Bug Reporting and Contribution

There are many MFA options and I have little free time. Please only submit feature requests if you plan to add them yourself and help maintain the project. If you need commercial support email info@burkesoftware.com.

Security vulnerabilities should be submitted on GitLab via a private issue.

### Features to consider contributing

- Prettier Django Admin integration
- Support for localhost dev without running HTTPS
- Email token MFA
- Unit tests for FIDO2
- Unit tests for Admin integration

## Support development

Maintaining django-rest-mfa takes time. Considering supporting it by:

- [Donating on liberapay](https://liberapay.com/burke-software/)
- Check out my other projects like [GlitchTip](https://glitchtip.com) error tracking or [Passit](https://passit.io) Password Manager.

This project started from a donation to Passit to add Yubikey support. Are you using it for your work? Do you want it maintained and not abandoned when I run out of time? Please donate financially or with time by helping with high quality documentation, unit tests, etc.

Commercial support is available - email info@burkesoftware.com
